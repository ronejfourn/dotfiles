vim.g.stripTrailingWhitespace = false
vim.g.notesPath = '~/Dropbox/notes'

require 'options'
require 'autocmd'
require 'command'
require 'keymaps'.setup 'regular'

vim.opt.rtp:append('~/.config/nvim/base16')
vim.cmd('colorscheme base16')
require 'plugins'

vim.g.loaded_perl_provider = 0
vim.g.loaded_ruby_provider = 0
vim.g.loaded_node_provider = 0
