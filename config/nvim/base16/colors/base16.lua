vim.cmd('syntax reset')
vim.cmd('highlight clear')
vim.cmd('set termguicolors')
vim.g.colors_name = "base16"

Colors = {
  base00 = "#1d2021",
  base01 = "#3c3836",
  base02 = "#504945",
  base03 = "#665c54",
  base04 = "#bdae93",
  base05 = "#d5c4a1",
  base06 = "#ebdbb2",
  base07 = "#fbf1c7",
  base08 = "#fb4934",
  base09 = "#fe8019",
  base0A = "#fabd2f",
  base0B = "#b8bb26",
  base0C = "#8ec07c",
  base0D = "#83a598",
  base0E = "#d3869b",
  base0F = "#d65d0e",
}

vim.g.terminal_color_0  = Colors.base00
vim.g.terminal_color_1  = Colors.base08
vim.g.terminal_color_2  = Colors.base0B
vim.g.terminal_color_3  = Colors.base0A
vim.g.terminal_color_4  = Colors.base0D
vim.g.terminal_color_5  = Colors.base0E
vim.g.terminal_color_6  = Colors.base0C
vim.g.terminal_color_7  = Colors.base05
vim.g.terminal_color_8  = Colors.base03
vim.g.terminal_color_9  = Colors.base08
vim.g.terminal_color_10 = Colors.base0B
vim.g.terminal_color_11 = Colors.base0A
vim.g.terminal_color_12 = Colors.base0D
vim.g.terminal_color_13 = Colors.base0E
vim.g.terminal_color_14 = Colors.base0C
vim.g.terminal_color_15 = Colors.base07

require("make_colorscheme").make(Colors)

return Colors
