vim.g.mapleader = ' '
vim.g.maplocalleader = ','

K = {}

K.etc = {
  nvim_autopairs = { fast_wrap = '<m-e>' },

  vim_lion = {
    right = '<leader>a',
    left  = '<leader>A',
  },

  nvim_comment = {
    line = '<leader>//',
    operator = '<leader>/',
  },

  oil = {
    ['<cr>'] = 'actions.select',
    ['<c-v>'] = 'actions.select_vsplit',
    ['<c-h>'] = 'actions.select_split',
    ['<c-t>'] = 'actions.select_tab',
    ['<c-p>'] = 'actions.preview',
    ['<c-c>'] = 'actions.close',
    ['<c-l>'] = 'actions.refresh',
    ['g?'] = 'actions.show_help',
    ['gp'] = 'actions.parent',
    ['gh'] = 'actions.toggle_hidden',
  },
}

K.regular = {
  n = {
    {'<leader>w', '<cmd>w<cr>'},
    {'<leader>q', '<cmd>q<cr>'},
    {'<leader>D', [["_D]]},
    {'<leader>C', [["_C]]},
    {'<leader>r', [["0yiw:%s/<c-r>0//g<left><left>]], {silent = false}},
    {'<leader>ew', '<cmd>ed.<cr>'},
    {'<leader>eh', '<cmd>sp.<cr>'},
    {'<leader>ev', '<cmd>vs.<cr>'},
    {'<leader>sh', '<cmd>sp<cr>'},
    {'<leader>sv', '<cmd>vs<cr>'},
    {'<leader>\\', '<cmd>Terminal<cr>'},
    {'<leader>n', '<cmd>Notes<cr>'},
    {'<leader>ws', '<cmd>ToggleStripTrailingWhitespace<cr>'},
    {'<m-e>', '<c-w>='},
    {'<m-h>', '<cmd>vertical resize +2<cr>'},
    {'<m-j>', '<cmd>resize +2<cr>'},
    {'<m-k>', '<cmd>resize -2<cr>'},
    {'<m-l>', '<cmd>vertical resize -2<cr>'},
    {'<tab>'  , ':bnext<cr>'},
    {'<s-tab>', ':bprev<cr>'},
    {'n', 'nzzzv'},
    {'N', 'Nzzzv'},
    {
      '<leader>cd',
      function()
        local oil = 'oil://'
        local exp = vim.fn.expand '%:p:h'
        local path = (exp:sub(0, #oil) == oil) and exp:sub(#oil+1) or exp
        vim.api.nvim_set_current_dir(path)
        vim.print('cd ' .. path)
        vim.defer_fn(function() vim.cmd "echo" end, 500)
      end
    },
  },

  c = {
    {'W' , 'w' , {silent = false}},
    {'Q' , 'q' , {silent = false}},
    {'Wq', 'wq', {silent = false}},
    {'WQ', 'wq', {silent = false}},
  },

  v = {
    {'<leader>p', [["_p]]},
    {'<leader>d', [["_d]]},
    {'<leader>c', [["_c]]},
    {'<leader>r', [["0y:%s/<c-r>0//g<left><left>]], {silent = false}},
    {'<', '<gv'},
    {'>', '>gv'},
    {'J', [[:move '>+1<cr>gv-gv]]},
    {'K', [[:move '<-2<cr>gv-gv]]},
  },

  [''] = {
    {'<c-u>', '<c-w>r'},
    {'<c-h>', '<c-w>h'},
    {'<c-j>', '<c-w>j'},
    {'<c-k>', '<c-w>k'},
    {'<c-l>', '<c-w>l'},
    {'<leader>', '<nop>'},
    {'<localleader>', '<nop>'},
  },

  [{'i', 't'}] = {
    {'<esc>', [[<c-\><c-n>]]},
    {'kj', [[<c-\><c-n>]]},
    {'<c-h>', '<left>'},
    {'<c-j>', '<down>'},
    {'<c-k>', '<up>'},
    {'<c-l>', '<right>'},
  },

  [{'', 'i', 't'}] = {
    {'<f1>', '<nop>'},
    {'<left>', '<nop>'},
    {'<right>', '<nop>'},
    {'<up>', '<nop>'},
    {'<down>', '<nop>'},
    {'<pageup>', '<nop>'},
    {'<pagedown>', '<nop>'},
  },
}

K.lsp = {
  n = {
    {'K' , '<cmd>lua vim.lsp.buf.hover()<cr>'},
    {'gd', '<cmd>lua vim.lsp.buf.definition()<cr>'},
    {'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>'},
    {'gr', '<cmd>lua vim.lsp.buf.references()<cr>'},
    {'gR', '<cmd>lua vim.lsp.buf.rename()<cr>'},
    {'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>'},
    {'gf', '<cmd>lua vim.lsp.buf.format()<cr>'},
    {'ga', '<cmd>lua vim.lsp.buf.code_action()<cr>'},
    {'<leader>dn', '<cmd>lua vim.diagnostic.goto_next()<cr>'},
    {'<leader>dp', '<cmd>lua vim.diagnostic.goto_prev()<cr>'},
    {'<leader>dq', '<cmd>lua vim.diagnostic.setqflist()<cr>'},
    {'<leader>df', '<cmd>lua vim.diagnostic.open_float()<cr>'},
  },

  i = {
    {'<c-s>', '<cmd>lua vim.lsp.buf.signature_help()<cr>'},
  },
}

K.telescope = {
  n = {
    {'<leader>ff', '<cmd>Telescope find_files<cr>'},
    {'<leader>fo', '<cmd>Telescope oldfiles<cr>'},
    {'<leader>fg', '<cmd>Telescope live_grep<cr>'},
    {'<leader>fG', '<cmd>Telescope grep_string<cr>'},
    {'<leader>fp', '<cmd>Telescope pickers<cr>'},
  }
}

K.neogit = {
  n = {
    {'<leader>g', '<cmd>Neogit<cr>'}
  }
}

K.setup = function(name, opts)
  opts = opts or {}
  if opts.silent == nil then opts.silent = true end
  if opts.remap  == nil then opts.remap = false end

  for modes, maps in pairs(K[name]) do
    for _, map in pairs(maps) do
      vim.schedule(function()
        local opt = opts
        if map[3] then opt = vim.tbl_extend('force', opts, map[3]) end
        vim.keymap.set(modes, map[1], map[2], opt)
      end)
    end
  end
end

return K
