local splitopen = function(buf, orient)
  if orient == 'h' then
    vim.cmd('botright sbuffer ' .. buf)
    vim.cmd('resize' .. math.floor(vim.o.lines / 3))
  else
    vim.cmd('vertical sbuffer ' .. buf)
    vim.cmd('vertical-resize' .. math.floor(2 * vim.o.columns / 3))
  end
end

local toggleablesplit = function(orient, callback)
  local buf = -1
  return function()
    if not vim.api.nvim_buf_is_valid(buf) then
      buf = vim.api.nvim_create_buf(false, true)
      splitopen(buf, orient)
      callback(buf)
    else
      local wn = vim.fn.bufwinid(buf)
      if wn == -1 then splitopen(buf, orient)
      else vim.api.nvim_win_close(wn, {force = true}) end
    end
  end
end

local termopen = function(buf)
  vim.cmd 'startinsert'
  local old_number = vim.wo.number
  local old_signcolumn = vim.wo.signcolumn
  local old_relativenumber = vim.wo.relativenumber
  vim.wo.number = false
  vim.wo.signcolumn = 'no'
  vim.wo.relativenumber = false
  vim.api.nvim_set_current_buf(buf)
  vim.fn.termopen(vim.o.shell, {on_exit = function ()
    vim.wo.relativenumber = old_relativenumber
    vim.wo.signcolumn = old_signcolumn
    vim.wo.number = old_number
    vim.cmd 'stopinsert'
    vim.api.nvim_buf_delete(buf, {force = true})
  end})
end

local commands = {
  ToggleStripTrailingWhitespace = {
    func = function() vim.g.stripTrailingWhitespace = not vim.g.stripTrailingWhitespace end
  },

  Terminal = { func = toggleablesplit('h', termopen) },

  Notes = {
    func = toggleablesplit('v', function(buf)
      vim.cmd('edit ' .. vim.g.notesPath)
      vim.bo[buf].buflisted = false
      vim.api.nvim_create_autocmd('BufWinLeave', {
        buffer = buf,
        command = [[silent! write!]]
      })
    end)
  },
}

for name, val in pairs(commands) do
  vim.api.nvim_create_user_command(name, val.func, val.args or {})
end
