local opt = vim.opt

vim.cmd('syntax off')

opt.shortmess:append "sI"
opt.fillchars     = {eob = " "}

opt.mouse         = ""
opt.clipboard     = "unnamedplus"

opt.wrap          = false
opt.scrolloff     = 4
opt.sidescrolloff = 4

opt.number        = true
opt.signcolumn    = "yes"
opt.cursorline    = true

opt.ignorecase    = true
opt.smartcase     = true
opt.incsearch     = true
opt.hlsearch      = false
opt.inccommand    = "split"

opt.shiftwidth    = 4
opt.tabstop       = 4
opt.shiftround    = true
opt.smarttab      = true
opt.expandtab     = true
opt.autoindent    = true
opt.smartindent   = true

opt.splitbelow    = true
opt.splitright    = true

opt.timeoutlen    = 300
opt.ttimeoutlen   = 100
opt.updatetime    = 300

opt.backup        = false
opt.swapfile      = false
opt.undofile      = true

opt.wildmode      = "longest,full"
opt.wildoptions   = "fuzzy"
opt.pumheight     = 10
opt.pumwidth      = 30
opt.pumblend      = 10
opt.winblend      = 10
