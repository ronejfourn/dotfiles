local has_words_before = function()
  unpack = unpack or table.unpack
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

C = {}
C.setup = function()
  local cmp = require 'cmp'
  local snp = require 'snippy'

  local cmp_autopairs = require('nvim-autopairs.completion.cmp')
  cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done())

  cmp.setup({
    view = { entries = 'native' },
    snippet = {
      expand = function(args)
        snp.expand_snippet(args.body)
      end,
    },

    mapping = {
      ['<cr>'] = cmp.mapping(function(fallback)
        if cmp.visible() and cmp.get_active_entry() then
          cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false })
        else fallback() end
      end, { 'i', 's' }),

      ['<tab>'] = cmp.mapping(function(fallback)
        if cmp.visible() then cmp.select_next_item()
        elseif snp.can_expand_or_advance() then snp.expand_or_advance()
        elseif has_words_before() then cmp.complete()
        else fallback() end
      end, { 'i', 's' }),

      ['<s-tab>'] = cmp.mapping(function(fallback)
        if cmp.visible() then cmp.select_prev_item()
        elseif snp.can_jump(-1) then snp.previous()
        else fallback() end
      end, { 'i', 's' }),
    },

    sources = cmp.config.sources({
      { name = 'snippy' },
      { name = 'nvim_lsp' },
      { name = 'buffer' },
      { name = 'path' },
    })
  })
end
return C
