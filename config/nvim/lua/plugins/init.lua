local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system { 'git', 'clone', '--filter=blob:none', 'https://github.com/folke/lazy.nvim.git', '--branch=stable', lazypath, }
end
vim.opt.rtp:prepend(lazypath)
local keymaps = require('keymaps')

local lazyload = function(plugin)
  vim.api.nvim_create_autocmd({ 'BufRead', 'BufWinEnter', 'BufNewFile' }, {
    group = vim.api.nvim_create_augroup('BufLazyLoad' .. plugin, {}),
    callback = function(arg)
      if arg.file ~= '' and not vim.startswith(arg.file, 'oil://') then
        vim.api.nvim_del_augroup_by_name('BufLazyLoad' .. plugin)
        vim.schedule(function() require('lazy').load {plugins = plugin} end)
      end
    end
  })
end

require('lazy').setup({
  {
    'akinsho/bufferline.nvim',
    lazy = false,
    main = 'bufferline',
    config = true,
  },

  {
    'stevearc/oil.nvim',
    lazy = false,
    opts = { keymaps = keymaps.etc.oil, use_default_keymaps = false },
  },

  {
    'TimUntersberger/neogit',
    cmd = 'Neogit',
    opts = { kind = 'replace' },
    init = function() keymaps.setup 'neogit' end,
  },

  {
    'nvim-telescope/telescope.nvim',
    cmd = 'Telescope',
    config = true,
    main = 'plugins.config_telescope',
    init = function() keymaps.setup 'telescope' end,
    dependencies = { { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' } },
  },

  {
    'tommcdo/vim-lion',
    init = function()
      vim.g.lion_squeeze_spaces = 1
      vim.g.lion_map_right = keymaps.etc.vim_lion.right
      vim.g.lion_map_left  = keymaps.etc.vim_lion.left
    end,
    keys = { keymaps.etc.vim_lion.right, keymaps.etc.vim_lion.left },
  },

  {
    'terrortylor/nvim-comment',
    main = 'nvim_comment',
    opts = {
      line_mapping = keymaps.etc.nvim_comment.line,
      operator_mapping = keymaps.etc.nvim_comment.operator,
    },
    keys = {
      { mode = 'n', keymaps.etc.nvim_comment.line },
      { mode = 'v', keymaps.etc.nvim_comment.operator },
    }
  },

  {
    'hrsh7th/nvim-cmp',
    event = 'InsertEnter',
    config = true,
    main = 'plugins.config_completion',
    dependencies = {
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-buffer',
      'dcampos/nvim-snippy',
      'dcampos/cmp-snippy',
      {
        'windwp/nvim-autopairs',
        opts = { fast_wrap = { map = keymaps.etc.nvim_autopairs.fast_wrap } },
      },
    },
  },

  {
    'neovim/nvim-lspconfig',
    config = true,
    main = 'plugins.config_lsp',
    init = function() lazyload 'nvim-lspconfig' end,
  },

  {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate',
    config = true,
    main = 'plugins.config_treesitter',
    init = function() lazyload 'nvim-treesitter' end,
  },

  {
    'sindrets/diffview.nvim',
    'nvim-lua/plenary.nvim',
    'nvim-tree/nvim-web-devicons',
  },

  {
    'williamboman/mason.nvim',
    cmd = { 'Mason' },
    opts = { PATH = 'skip', },
    init = function()
      vim.env.PATH = vim.env.PATH .. ':' .. vim.fn.stdpath 'data' .. '/mason/bin'
    end,
  },
}, {
  performance = {
    rtp = {
      disabled_plugins = {
        'matchit', 'matchparen', 'netrw', 'netrwPlugin',
        'netrwSettings', 'netrwFileHandlers', 'gzip', 'zip',
        'zipPlugin', 'tar', 'tarPlugin', 'getscript',
        'getscriptPlugin', 'vimball', 'vimballPlugin', 'tohtml',
        '2html_plugin', 'logipat', 'rrhelper', 'rplugin',
        'editorconfig', 'man', 'nvim',
      },
    },
  },
  defaults = {lazy = true},
})
