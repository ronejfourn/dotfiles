local installed = {
  pylsp  = {},
  clangd = {},
  dartls = {},
  lua_ls = {
    settings = {
      Lua = {diagnostics = {
        globals = { 'vim' }
      }}
    }
  },
}

local config_lsp = vim.api.nvim_create_augroup('config_lsp', {clear = true})

local on_attach = function(client, bufnr)
  vim.api.nvim_create_autocmd(
    { 'CursorHold' }, {
    buffer = bufnr,
    group = config_lsp,
    callback = vim.lsp.buf.document_highlight
  })

  vim.api.nvim_create_autocmd(
    { 'CursorMoved', 'BufLeave', 'FocusLost', 'InsertEnter', 'WinLeave', 'CmdlineEnter' }, {
    buffer = bufnr,
    group = config_lsp,
    callback = function(ev)
      vim.lsp.buf.clear_references()
      if ev.event == 'CmdlineEnter' then vim.cmd('redraw') end
    end
  })

  client.server_capabilities.semanticTokensProvider = nil
  require('keymaps').setup.lsp(bufnr)
end

local root_dir = function(fname)
  local util = require('lspconfig').util
  local path = util.path
  local filename = path.is_absolute(fname) and fname or path.join(vim.loop.cwd(), fname)
  return util.root_pattern('.git')(filename) or path.dirname(filename)
end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem = {
  snippetSupport = true,
  preselectSupport = true,
  labelDetailsSupport = true,
  insertReplaceSupport = true,
  commitCharactersSupport = true,
  resolveSupport = {
    properties = {
      'documentation',
      'detail',
      'additionalTextEdits',
    },
  },
}

return {
  setup = function()
    local keymaps = require 'keymaps'
    keymaps.setup 'lsp'

    local signs = {
      DiagnosticSignError = '',
      DiagnosticSignWarn  = '',
      DiagnosticSignHint  = '',
      DiagnosticSignInfo  = '󰌵',
    }

    for name, sign in pairs(signs) do
      vim.fn.sign_define(name, {text = sign, numhl = name, texthl = name})
    end

    vim.diagnostic.config({
      signs = true,
      virtual_text = true,
      update_in_insert = true,
      float = { focusable = false },
    })

    for server, config in pairs(installed) do
      config.root_dir = config.root_dir or root_dir
      config.on_attach = config.on_attach or on_attach
      config.capabilities = config.capabilities or capabilities
      require('lspconfig')[server].setup(config)
    end

    vim.cmd 'silent! do FileType'
  end
}
