T = {}

T.setup = function()
  vim.o.foldenable = false
  vim.o.foldmethod = 'expr'
  vim.o.foldexpr = 'nvim_treesitter#foldexpr()'
  require('nvim-treesitter.configs').setup {
    ensure_installed = 'all',
    highlight = {
      enable = true,
    },
  }
end

return T
