local setrelnum = function(val)
  return function(ev)
    if vim.o.number or vim.o.relativenumber then
      vim.o.relativenumber = val
      if (ev.event == 'CmdlineEnter') then
        vim.cmd('redraw')
      end
    end
  end
end

local autocmds = {
  {
    events = 'FileType',
    pattern = {'lua', 'scheme', 'tex', 'dart'},
    callback = function(_)
      vim.opt_local.tabstop = 2
      vim.opt_local.shiftwidth = 2
    end
  },

  {
    events = 'BufWritePre',
    callback = function()
      if vim.g.stripTrailingWhitespace and vim.bo.modifiable then
        local v = vim.fn.winsaveview()
        vim.cmd [[%s/\s\+$//e]]
        vim.notify('Stripped Trailing Whitespaces')
        vim.fn.winrestview(v)
      end
    end
  },

  {
    events = 'CmdLineLeave',
    callback = function()
      vim.defer_fn(function() vim.cmd "echo" end, 500)
    end
  },

  {
    events = 'Signal',
    pattern = 'SIGUSR1',
    command = [[so $MYVIMRC]],
  },

  {
    events = 'TextYankPost',
    command = 'silent! lua vim.highlight.on_yank()'
  },

  {
    events = { 'BufEnter', 'FocusGained', 'InsertLeave', 'WinEnter', 'CmdlineLeave' },
    callback = setrelnum(true),
  },

  {
    events = { 'BufLeave', 'FocusLost', 'InsertEnter', 'WinLeave', 'CmdlineEnter' },
    callback = setrelnum(false),
  },

  {
    events = 'FileType',
    pattern = {'qf'},
    callback = function() vim.opt_local.buflisted = false end
  },
}

local augroup = vim.api.nvim_create_augroup('DefaultAugroup', {})
for _, ac in pairs(autocmds) do
  local ev = ac.events
  ac.events = nil
  ac.group = augroup
  vim.api.nvim_create_autocmd(ev, ac)
end
